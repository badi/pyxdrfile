from distutils.core import setup, Extension

import os
import glob

def modules():
    for p in glob.glob('xdr/*.py'):
        name, _ = os.path.splitext(p)
        yield name.replace('/', '.')


_xdrprefix = os.path.join('external','xdrfile-1.1b')
_xdrsrc    = map(lambda p: os.path.join(_xdrprefix, 'src', p),
                 ['trr2xtc.c',
                  'xdrfile.c',
                  'xdrfile_trr.c',
                  'xdrfile_xtc.c'])

libxdrfile = Extension('xdr._libxdrfile',
                       sources            = _xdrsrc,
                       extra_compile_args = [],
                       extra_link_args    = ["--enable-shared"],
                       include_dirs       = [os.path.join(_xdrprefix, 'include')]         )


setup(name                                = 'pyxdrfile',
      version                             = '1.4',
      py_modules                          = list(modules()),
      ext_modules                         = [libxdrfile],
      author                              = "Badi' Abdul-Wahid",
      author_email                        = 'abdulwahidc@gmail.com',
      license                             = 'LGPL',
      description                         = 'Python bindings to the GROMACS xdrfile C library'
      )
