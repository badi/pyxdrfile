
import tests
import os, sys

import xdr

def read_xtc_natoms():
    xtcname  = os.path.join(tests.mydir, 'test.xtc')
    expected = 582
    natoms   = xdr.libxdrfile.read_xtc_natoms(xtcname)

    if natoms == expected:
        return True, ()
    else:
        return False, 'Expected %s got %s' % (expected, natoms)

def read_xtc():
    xtcname = os.path.join(tests.mydir, 'test.xtc')
    fd      = xdr.libxdrfile.xdrfile_open(xtcname, 'r')

    try:
        for i in xrange(1000):
            res = xdr.libxdrfile.read_xtc(fd, xtcname, frame=i)
    except xdr.libxdrfile.XDREOF: pass
    xdr.libxdrfile.xdrfile_close(fd)

    return True, ()

def write_xtc():
    xtcin  = os.path.join(tests.mydir, 'test.xtc')
    xtcout = os.path.join(tests.mydir, 'testout.xtc')

    fd_in  = xdr.libxdrfile.xdrfile_open(xtcin, 'r')
    fd_out = xdr.libxdrfile.xdrfile_open(xtcout, 'w')

    try:
        for i in xrange(1000):
            res                = xdr.libxdrfile.read_xtc(fd_in, xtcin, frame=i)
            coords, step, time = res['coords'], res['step'], res['time']

            xdr.libxdrfile.write_xtc(fd_out, coords, step, time)
    except xdr.libxdrfile.XDREOF: pass
    xdr.libxdrfile.xdrfile_close(fd_in)
    xdr.libxdrfile.xdrfile_close(fd_out)

    return True, ()

def read_trr_natoms():
    trrname = os.path.join(tests.mydir, 'test.trr')
    expected = 582
    natoms = xdr.libxdrfile.read_trr_natoms(trrname)

    if natoms == expected: return True, ()
    else: return False, 'Expected %s got %s from %s' % (expected, natoms, trrname)

def read_natoms():
    trrname = os.path.join(tests.mydir, 'test.trr')
    xtcname = os.path.join(tests.mydir, 'test.xtc')
    expected = 582

    ntrr = xdr.libxdrfile.read_natoms(trrname)
    nxtc = xdr.libxdrfile.read_natoms(xtcname)

    if ntrr == expected and nxtc == expected:
        return True, ()
    elif not ntrr == expected:
        return False, 'Got %s atoms, expected %s, in %s' % (ntrr, expected, trrname)
    elif not nxtc == expected:
        return False, 'Got %s atoms, expected %s, in %s' % (nxtc, expected, xtcname)
    else:
        raise ValueError, 'Logic failed'

def read_trr():
    trrname = os.path.join(tests.mydir, 'test.trr')
    fd = xdr.libxdrfile.xdrfile_open(trrname, 'r')

    try:
        for fr in xrange(1000):
            res    = xdr.libxdrfile.read_trr(fd, trrname)

            coords = res['coords']
            vels   = res['velocities']
            forces = res['forces']
            box    = res['box']
            step   = res['step']
            time   = res['time']
            lmbda  = res['lambda']
            natoms = res['natoms']


    except xdr.libxdrfile.XDRInt: pass
    finally: xdr.libxdrfile.xdrfile_close(fd)

    return True, ()

def write_trr():
    trrname = os.path.join(tests.mydir, 'test.trr')
    trrout  = os.path.join('/', 'tmp', 'test.trr')

    fdin    = xdr.libxdrfile.xdrfile_open(trrname, 'r')
    fdout   = xdr.libxdrfile.xdrfile_open(trrout, 'w')

    try:
        for fr in xrange(1000):
            res = xdr.libxdrfile.read_trr(fdin, trrname)
            xdr.libxdrfile.write_trr(fdout, fr, fr, 0., res['box'],
                                     res['coords'], res['velocities'], res['forces'])
    finally:
        print 'hello'
        xdr.libxdrfile.xdrfile_close(fdin)
        xdr.libxdrfile.xdrfile_close(fdout)

    return True, ()


tests.run(read_xtc_natoms,
          read_xtc,
          write_xtc,
          read_trr_natoms,
          read_natoms,
          read_trr)
