
import os, sys, traceback

mydir  = os.path.dirname(os.path.abspath(sys.argv[0]))
srcdir = os.path.dirname(mydir)

sys.path = [srcdir] + sys.path



def run(*tests):
    for t in tests:
        print 'Running:', t.func_name
        try:
            ok, msg = t()

            if ok: print 'OK'
            else:  print 'FAILURE:', msg

        except Exception, e:
            traceback.print_exc()
            print 'FAILURE:', e
