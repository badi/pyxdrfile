

import numpy as np

import ctypes

try:
    _libxdrfile = ctypes.CDLL('_libxdrfile.so')
except:
    raise ImportError, 'Could not find shared library _libxdrfile.so in LD_LIBRARY_PATH'


import os


class XDRError        (Exception) : pass
class XDRHeader       (XDRError)  : pass
class XDRString       (XDRError)  : pass
class XDRDouble       (XDRError)  : pass
class XDRInt          (XDRError)  : pass
class XDRFloat        (XDRError)  : pass
class XDRUInt         (XDRError)  : pass
class XDRC3DC         (XDRError)  : pass
class XDRClose        (XDRError)  : pass
class XDRMagic        (XDRError)  : pass
class XDRNoMem        (XDRError)  : pass
class XDREOF          (XDRError)  : pass
class XDRFileNotFound (XDRError)  : pass



class XDRDefaults(object):
    dim = 3

    ##################################################################
    ### IMPORTANT                                                  ###
    ### The box and coords numpy.dtype MUST be float32 in order to ###
    ### correctly read from xtc files                              ###
    ##################################################################

    @classmethod
    def box(cls):
        return np.zeros((3,3), dtype='float32')

    @classmethod
    def coords(cls, natoms):
        return np.zeros((natoms, cls.dim), dtype='float32')

    @classmethod
    def velocities(cls, natoms):
        return np.zeros((natoms, cls.dim), dtype='float32')

    @classmethod
    def forces(cls, natoms):
        return np.zeros((natoms, cls.dim), dtype='float32')

    @classmethod
    def natoms(cls):
        return ctypes.c_int()

    @classmethod
    def step(cls):
        return ctypes.c_int()

    @classmethod
    def time(cls):
        return ctypes.c_float()

    @classmethod
    def precision(cls):
        return ctypes.c_float(1000.)


def dereference(p, kind='pointer'):
    """
    dereference pointers and references

    Parameters:
    *p*: result of ctypes.pointer or ctypes.byref

    Keywords:
    *kind*: enum {pointer | ref}
    """
    if kind == 'pointer':
        return p.contents.value
    elif kind == 'ref':
        return p._obj.value

def numpy_ndarray_to_ctypes_pointer(arr, dtype='float'):
    types = {'float' : ctypes.c_float,
             'int'   : ctypes.c_int  }
    typ = types[dtype]

    return arr.ctypes.data_as(ctypes.POINTER(typ))

class EXDR:
    ok           = 0     # OK
    header       = 1     # Header
    string       = 2     # String
    double       = 3     # Double
    int          = 4     # Integer
    float        = 5     # Float
    uint         = 6     # Unsigned integer
    c3dc         = 7     # Compressed 3d coordinate
    close        = 8     # Closing file
    magic        = 9     # Magic number
    nomem        = 10    # Not enough memory
    endoffile    = 11    # End of file
    filenotfound = 12    # File not found

    @classmethod
    def description(cls, code):
        descs = {cls.ok           : 'OK',
                 cls.header       : 'Header',
                 cls.string       : 'String',
                 cls.double       : 'Double',
                 cls.int          : 'Integer',
                 cls.float        : 'Float',
                 cls.uint         : 'Unsigned integer',
                 cls.c3dc         : 'Compressed 3D coordinate',
                 cls.close        : 'Closing file',
                 cls.magic        : 'Magic number',
                 cls.nomem        : 'Not enough memory',
                 cls.endoffile    : 'End of file',
                 cls.filenotfound : 'File not found'}
        return descs[code] + ': %s' % code

    @classmethod
    def exception(cls, code):
        classes = {cls.header       : XDRHeader,
                   cls.string       : XDRString,
                   cls.double       : XDRDouble,
                   cls.int          : XDRInt,
                   cls.float        : XDRFloat,
                   cls.uint         : XDRUInt,
                   cls.c3dc         : XDRC3DC,
                   cls.close        : XDRClose,
                   cls.magic        : XDRMagic,
                   cls.nomem        : XDRNoMem,
                   cls.endoffile    : XDREOF,
                   cls.filenotfound : XDRFileNotFound}
        return classes[code]

    @classmethod
    def check_ok(cls, code):
        if not code == cls.ok:
            ex = cls.exception(code)
            raise ex, cls.description(code)



def read_trr(filedesc, name):
    """
    :Returns: a dict with following keys:
      *coords* : numpy ``array((natoms, 3), dtype='float32')``
      *velocities* : numpy ``array((natoms, 3), dtype='float32')``
      *forces* : numpy ``array((natoms, 3), dtype='float32')``
      *box* : numpy ``array((3, 3), dtype='float32')``
      *step* : int
      *time* : float : in units of picoseconds
      *lambda* : float
      *natoms* : int
     """

    byref  = ctypes.byref

    natoms = read_trr_natoms(name)

    step   = XDRDefaults.step()
    time   = XDRDefaults.time()
    lmbda  = ctypes.c_float()

    box    = XDRDefaults.box()
    x      = XDRDefaults.coords(natoms)
    v      = XDRDefaults.velocities(natoms)
    f      = XDRDefaults.forces(natoms)

    code = _libxdrfile.read_trr(filedesc,
                                natoms,
                                byref(step),
                                byref(time),
                                byref(lmbda),
                                numpy_ndarray_to_ctypes_pointer(box),
                                numpy_ndarray_to_ctypes_pointer(x),
                                numpy_ndarray_to_ctypes_pointer(v),
                                numpy_ndarray_to_ctypes_pointer(f))

    EXDR.check_ok(code)
    return {'coords'     : x,
            'velocities' : v,
            'forces'     : f,
            'box'        : box,
            'step'       : step.value,
            'time'       : time.value,
            'lambda'     : lmbda.value,
            'natoms'     : natoms}


def read_natoms(name, kind=None):
    """
    Read the number of atom for the structure in the trajectory

    :Argument:
      *name* : string : the path to the xtc or trr file
      *kind* = None : {xtc|trr}

    :Returns: int
    """

    if   kind == 'trr' or os.path.splitext(name)[-1] == '.trr': readnatoms = _libxdrfile.read_trr_natoms
    elif kind == 'xtc' or os.path.splitext(name)[-1] == '.xtc': readnatoms = _libxdrfile.read_xtc_natoms
    else: raise ValueError, 'Unknown file type: %s (kind = %s)' % (name, kind)

    natoms = XDRDefaults.natoms()
    code   = readnatoms(name, ctypes.byref(natoms))
    EXDR.check_ok(code)
    return natoms.value


def read_trr_natoms(name):
    """
    Read the number of atoms in each frame

    Parameters:
    *name* : string

    Returns: int : the number of atoms
    """
    return read_natoms(name, kind='trr')


def read_xtc(filedesc, name, frame=0, precision=XDRDefaults.precision()):
    """
    Thin wrapper over xdrfile.read_xtc

    Parameters:
    *filedesc* : file descriptor : returned by call to _xdrfile.xdrfile_open
    *name* : str : path to the xtc file

    Keywords
    *frame* : 0 : int : which frame to read
    *precition* : XDRDefaults.precision : ctypes.c_int

    Return : dict
    keys = natoms : int
           step   : int                                               : the frame number
           time   : float                                             : the time at the frame in ps
           coords : numpy.ndarray : shape = (natoms,3), dtype = float : the atom coordinates 
           box    : numpy.ndarray : shape = (3,3),      dtype = float : the box
           prec   : float                                             : the xtc precision
    """

    natoms = read_xtc_natoms(name)
    step   = ctypes.c_int(frame)
    time   = XDRDefaults.time()
    coords = XDRDefaults.coords(natoms)
    box    = XDRDefaults.box()
    prec   = XDRDefaults.precision()

    code   = _libxdrfile.read_xtc(filedesc,
                                  natoms,
                                  ctypes.pointer(step),
                                  ctypes.pointer(time),
                                  numpy_ndarray_to_ctypes_pointer(box),
                                  numpy_ndarray_to_ctypes_pointer(coords),
                                  ctypes.pointer(prec))

    EXDR.check_ok(code)
    return {'natoms' : natoms,
            'step'   : step.value,
            'time'   : time.value,
            'coords' : coords,
            'box'    : box,
            'prec'   : prec.value}


def read_xtc_natoms(name):
    """
    Read the number of atoms in each frame

    Parameters:
    *name* : string

    Returns: int : the number of atoms
    """
    return read_natoms(name, kind='xtc')


# TODO: this function aborts for some reason
def write_trr(filedesc, step, time, lmbda, box, coords, vels, forces):

    byref   = ctypes.byref

    _natoms = len(coords)
    _step   = ctypes.c_int(step)
    _time   = ctypes.c_float(time)
    _lambda = ctypes.c_float(lmbda)
    _box    = numpy_ndarray_to_ctypes_pointer(box)
    _coords = numpy_ndarray_to_ctypes_pointer(coords)
    _vels   = numpy_ndarray_to_ctypes_pointer(vels)
    _forces = numpy_ndarray_to_ctypes_pointer(forces)

    code = _libxdrfile.write_xtc(filedesc,
                                 _natoms,
                                 byref(_step),
                                 byref(_time),
                                 byref(_lambda),
                                 _box,
                                 _coords,
                                 _vels,
                                 _forces)
    EXDR.check_ok(code)

def write_xtc(filedesc, coords, step, time, box=XDRDefaults.box(), precision=XDRDefaults.precision()):
    """
    This wrapper over libxdrfile.write_xtc

    Parameters:
    *filedesc* : file descriptor                                      : return by call to _xdrfile.xdrfile_open
    *coords*   : numpy.ndarray   : shape = (natoms, 3), dtype = float : atom coordinates for a frame
    *step*     : int                                                  : the step
    *time*     : float                                                : the time in ps

    Keywords
    *box* : XDRDefaults.box
    *precision* : XDRDefaults.precision

    Return: no return value
    """

    natoms = len(coords)
    step = ctypes.c_int(step)
    time = ctypes.c_float(time)

    code = _libxdrfile.write_xtc(filedesc,
                                 natoms,
                                 step,
                                 time,
                                 numpy_ndarray_to_ctypes_pointer(box),
                                 numpy_ndarray_to_ctypes_pointer(coords),
                                 precision)

    EXDR.check_ok(code)



def xdrfile_open(name, mode):
    return _libxdrfile.xdrfile_open(name, mode)

def xdrfile_close(filedesc):
    code = _libxdrfile.xdrfile_close(filedesc)
    EXDR.check_ok(code)
