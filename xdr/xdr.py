
import _xdrfile

import numpy as np

import os
import sys
import ctypes


class IOMode:
    write  = 'w'
    read   = 'r'


class XDRError  (Exception): pass
class XDRExists (XDRError): pass

class Frame(object):

    def __init__(self, values, step, time, box=None):

        self.values = values
        self.step   = step
        self.time   = time
        self.box    = box if box is not None else _xdrfile.XDRDefaults.box()

    def __getitem__(self, ix):
        return self.values[ix]

    def _build_str(self):
        return 'natoms=%s, step=%s, time=%s' % (len(self.values), self.step, self.time)

    def __str__(self):
        return '%s{%s}' % (self.__class__.__name__, self._build_str())


class XTCFrame(Frame):
    """
    Contains the data of a single frame

    Attributes:
    *coords*    : (N,3) shape numpy.ndarray of atom coordinates
    *step*      : float
    *time*      : float
    *precision* : float
    *box*       : (3,3) numpy.ndarray of the simulation box
    """

    def __init__(self, coords, step, time, box=None, precision=None):
        Frame.__init__(self, coords, step, time, box=box)
        self.precision = precision if precision is not None else _xdrfile.XDRDefaults.precision().value
        self.coords    = coords

    def _build_str(self):
        return ', '.join([Frame._build_str(self),
                          'precision=%s' % self.precision])

class TRRFrame(Frame):
    def __init__(self, coords, step, time, box=None, vels=None, forces=None, lmbda=None):
        Frame.__init__(self, coords, step, time, box=box)
        self.coords     = coords
        self.velocities = vels
        self.forces     = forces
        self.lmbda      = lmbda

    def _build_str(self):
        return ', '.join([Frame._build_str(self),
                          'velocities=%s, forces=%s, lambda=%s' % \
                              (self.velocities is not None,
                               self.forces is not None,
                               self.lmbda)])


def _xtcframe_from_xdrfile_read(d):
    return XTCFrame(d['coords'],
                    d['step'],
                    d['time'],
                    precision = d['prec'],
                    box = d['box'])

def _trrframe_from_xdrfile_read(d):
    return TRRFrame(d['coords'],
                    d['step'],
                    d['time'],
                    box=d['box'],
                    vels   = d['velocities'],
                    forces = d['forces'],
                    lmbda  = d['lambda'])


class Trajfile(object):

    def __init__(self, name,
                 mode=IOMode.read,
                 overwrite=False):

        self.name       = name
        self.mode       = mode
        self.overwrite  = overwrite

        if self.mode == IOMode.read:
            self.natoms = _xdrfile.read_natoms(self.name)
        else:
            self.atoms  = None

        self.closed     = True

        self._fd_xdr    = None

    def open(self):
        if self.mode == IOMode.write and not self.overwrite:
            raise IOError, 'File %s already exists' % self.name

        self._fd_xdr = _xdrfile.xdrfile_open(self.name, self.mode)
        self.closed = False

    def read(self, frame):
        """
        Read a *Frame* from the xtc file

        Parameters:
        *frame* : int : which step in the xtc to read

        Returns : *Frame* subclass
        """
        raise NotImplementedError

    def write(self, frame):
        """
        Write a *Frame* to the xtc file

        Parameters:
        *frame* : Frame subclass
        """
        raise NotImplementedError


    def close(self):
        _xdrfile.xdrfile_close(self._fd_xdr)
        self.closed = True

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()

    def __iter__(self):
        try:
            fr = 0
            while True:
                yield self.read(fr)
                fr += 1
        except _xdrfile.XDREOF: raise StopIteration



class XTCFile(Trajfile):

    def read(self, frame):
        """Override Trajfile.read"""
        res = _xdrfile.read_xtc(self._fd_xdr, self.name, frame=frame)
        fr  = _xtcframe_from_xdrfile_read(res)
        return fr

    def write(self, frame):
        """Override Trajfile.write"""
        _xdrfile.write_xtc(self._fd_xdr, frame.coords, frame.step, frame.time, frame.box,
                           ctypes.c_float(frame.precision))


class TRRFile(Trajfile):

    def read(self, frame=None):
        """Override Trajfile.read"""
        try:
            res = _xdrfile.read_trr(self._fd_xdr, self.name)
        except _xdrfile.XDRInt: raise _xdrfile.XDREOF
        fr  = _trrframe_from_xdrfile_read(res)
        return fr

